#!/usr/bin/env bash

die() {
    echo "$@" >&2
    exit 1
}
cleanup() {
    [[ -n "${create_sql}" ]] && rm -f "${create_sql}"
    [[ -n "${use_database}" ]] && psql -At -d postgres -c "DROP DATABASE IF EXISTS ${use_database}"
}
psql() {
    command psql -qX -v ON_ERROR_STOP=1 "$@"
}
fatal_psql() {
    command psql -qX -v ON_ERROR_STOP=1 "$@" || die "Can't run [$*] via psql?!"
}
find_max_length() {
    local max=0
    local t
    for t in "$@"
    do
        (( "${#t}" > max )) && max="${#t}"
    done
    echo "${max}"
}
load_preps() {
    local use_db="$1"
    local group_name="$2"
    local max_len prep_file
    local -a prep_files
    local output

    [[ -d "tests/${group_name}/prep" ]] || return 0
    mapfile -t prep_files < <( find "tests/${group_name}/prep" -mindepth 1 -maxdepth 1 -type f -name '*.sql' -printf '%P\n' | sort -V )
    (( 0 == "${#prep_files[@]}" )) && return 0

    printf -- '- prep files:\n'
    max_len="$( find_max_length "${prep_files[@]}" )"
    for prep_file in "${prep_files[@]}"
    do
        printf -- "  - %-${max_len}s : " "${prep_file}"
        if output="$( psql -d "${use_db}" -At -f "tests/${group_name}/prep/${prep_file}" 2>&1 )"
        then
            echo 'OK'
            continue
        fi
        echo 'FAIL'
        printf "Output from loading:\n%s\n" "${output}"
        return 1
    done
    return 0
}

run_group_tests() {
    local use_db="$1"
    local group_name="$2"
    local max_len test_file
    local -a test_files
    local real_output fun_output
    local fails=0

    mapfile -t test_files < <(
        find "tests/${group_name}" -mindepth 1 -maxdepth 1 -type f -regex '.*/[0-9]+\..*\.\(real\|function\)\.sql' -printf '%P\n' |
            sed 's/\.\(real\|function\)\.sql$//' |
            sort -uV
    )
    (( 0 == "${#test_files[@]}" )) && return 0
    (( test_count += "${#test_files[@]}" ))

    printf -- '- tests:\n'
    max_len="$( find_max_length "${test_files[@]}" )"
    for test_file in "${test_files[@]}"
    do
        (( test_ran++ ))
        printf -- "  - %-${max_len}s : " "${test_file}"
        if ! real_output="$( psql -d "${use_db}" -f "tests/${group_name}/${test_file}.real.sql" | sed 's/[[:space:]]*$//' )"
        then
            echo 'FAIL'
            (( test_fail++ ))
            fails=1
            continue
        fi
        if ! fun_output="$( psql -At -d "${use_db}" -f "tests/${group_name}/${test_file}.function.sql" | sed 's/[[:space:]]*$//' )"
        then
            echo 'FAIL'
            (( test_fail++ ))
            fails=1
            continue
        fi
        if [[ "${real_output}" == "${fun_output}" ]]
        then
            echo 'OK'
            (( test_ok++ ))
            continue
        fi
        echo 'FAIL'
        (( test_fail++ ))
        fails=1
        printf 'Expected output:\n===\n%s\n===\n' "${real_output}"
        printf 'Got output:\n===\n%s\n===\n' "${fun_output}"
        continue
    done

    return "${fails}"
}

run_group() {
    local use_db="$1"
    local group_name="$2"
    load_preps "${use_db}" "${group_name}" || return 1
    run_group_tests "${use_db}" "${group_name}" || return 1
    return 0
}

cd "$( dirname "$( dirname "$( readlink -f "$0" )" )" )" || die "Can't go to top dir of project?!"

printf 'Running tests on %s\n\n' "$( psql -Atc 'select version()' )"

declare -g test_count=0
declare -g tests_ran=0
declare -g tests_ok=0
declare -g tests_fail=0

create_sql="$( mktemp )"
trap cleanup EXIT

./scripts/build.full.sql.sh -o "${create_sql}" > /dev/null

mapfile -t groups < <( find tests -mindepth 1 -maxdepth 1 -type d -printf '%P\n' | sort -V )

any_fails=0

for i in "${!groups[@]}"
do
    group_name="${groups[$i]}"
    printf 'Group %d of %d : %s:\n' "$(( i + 1 ))" "${#groups[@]}" "${group_name}"
    use_database="psql_backslash_test_$( tr -cd 'a-z' < /dev/urandom | head -c 10 )"
    fatal_psql -Atc "create database ${use_database}"
    fatal_psql -d "${use_database}" -At -f "${create_sql}"
    run_group "${use_database}" "${group_name}" || any_fails=1
    fatal_psql -Atc "drop database ${use_database}"
    unset use_database
done

printf '\n Run stats:\n'
printf 'Groups       : %d\n' "${#groups[@]}"
printf 'Tests        : %d\n' "${test_count}"
printf 'Tests ran    : %d\n' "${test_ran}"
printf 'Tests ok     : %d\n' "${test_ok}"
printf 'Tests failed : %d\n' "${test_fail}"
printf '\n'

if (( 0 == any_fails ))
then
    echo "All OK"
    exit 0
fi

echo "There were problems!"
exit 1
